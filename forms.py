# -*- coding: UTF-8 -*-
"""(forms.py) EADHeaderForm"""
from flask_wtf import Form 

from wtforms import StringField

from wtforms.validators import InputRequired, Length

class EADHeaderForm(Form):
   recordid = StringField(u'recordid:*', validators=[InputRequired()])
   otherrecordid = StringField(u'otherrecordid:')
   representation = StringField(u'representation:')
   filedesc = 	StringField(u'filedesc:*', validators=[InputRequired()])
   maintenancestatus = 	StringField(u'maintenancestatus:*', validators=[InputRequired()])
   publicationstatus = 	StringField(u'publicationstatus:*', validators=[InputRequired()])
   maintenanceagency = 	StringField(u'maintenanceagency:*', validators=[InputRequired()])
   languagedeclaration = 	StringField(u'languagedeclaration:*', validators=[InputRequired()])
   conventiondeclaration = 	StringField(u'conventiondeclaration:')
   localtypedeclaration = 	StringField(u'localtypedeclaration:')
   localcontrol = 	StringField(u'localcontrol:')
   maintenancehistory = 	StringField(u'maintenancehistory:')
   sources = 	StringField(u'sources:')